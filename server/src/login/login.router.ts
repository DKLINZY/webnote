import express, { Request, Response } from "express";
import * as LoginService from "./login.service";
import {
  authenticate,
  decodeToken,
  DecodeUser,
} from "../middleware/auth.middleware";
import { User } from "../models/user.model";

export const loginRouter = express.Router();

loginRouter.get("/", authenticate, async (req: Request, res: Response) => {
  try {
    const decodeUser: DecodeUser = decodeToken(
      req.headers.authorization as string
    );
    const userAuth = await LoginService.checkLogin({
      username: decodeUser.username,
      role: decodeUser.role,
      token: req.headers.authorization as string,
    });
    if (userAuth) res.status(200).send({ userAuth });
  } catch (e) {
    res.status(500).send(e.message);
  }
});

loginRouter.post("/", async (req: Request, res: Response) => {
  try {
    const user: User = req.body;
    if (!("username" in user) || !("password" in user))
      throw new Error("username or password not found");
    const userAuth = await LoginService.login(user);
    if (userAuth) {
      res.status(200).json({ userAuth });
    } else {
      res.status(404).send({ errMsg: "Invalid Username or Password" });
    }
  } catch (e) {
    res.status(500).send({ errMsg: e.message });
  }
});
