import { User, UserAuth } from "../models/user.model";
import UserController from "../controller/user.controller";
import { TOKEN_SECRET } from "../config";

import * as jwt from "jsonwebtoken";

const checkLogin = async ({
  username,
  role,
  token,
}: {
  username: string;
  role: string;
  token: string;
}): Promise<UserAuth | null> => {
  const user = await UserController.FindUser({ username, role });
  if (user) {
    const userAuth: UserAuth = {
      _id: user.id,
      username,
      token: token.split(" ")[1],
      role,
    };
    return userAuth;
  } else {
    return null;
  }
};

const login = async ({
  username,
  password,
}: User): Promise<UserAuth | null> => {
  const user = await UserController.FindUser({ username, password });
  if (user) {
    const userAuth: UserAuth = {
      _id: user._id,
      username: user.username,
      token: jwt.sign(
        { username: user.username, role: user.role },
        TOKEN_SECRET
      ),
      role: user.role,
    };
    return userAuth;
  } else {
    return null;
  }
};

export { checkLogin, login };
