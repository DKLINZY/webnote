import mongoose, { Schema, Document } from "mongoose";

export interface UserAuth {
  _id: string;
  username: string;
  token: string;
  role: string;
}

export interface User {
  username: string;
  password: string;
}

export interface UserDocument extends User, Document {
  role: string;
}

const UserSchema: Schema = new Schema({
  username: { type: String, required: true },
  password: { type: String, required: true },
  role: { type: String, required: true },
});

export default mongoose.model<UserDocument>("User", UserSchema);
