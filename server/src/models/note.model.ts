import mongoose, { Schema, Document } from "mongoose";

export interface BaseNote {
  title: string;
  content?: string;
  createAt: Date;
  updateAt?: Date;
}

export interface Note extends BaseNote {
  id: number;
}

export interface NoteDocument extends BaseNote, Document {
  user: { username: string; role: string };
}

const NoteSchema: Schema = new Schema({
  title: { type: String, required: true },
  content: { type: String },
  createAt: { type: Date, required: true },
  updateAt: { type: Date },
  user: {
    username: { type: String, required: true },
    role: { type: String, required: true },
  },
});

export default mongoose.model<NoteDocument>("Note", NoteSchema);
