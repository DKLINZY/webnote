import UserModel, { UserDocument } from "../models/user.model";
import { CreateQuery, UpdateQuery, FilterQuery } from "mongoose";

const CreateUser = async ({
  username,
  password,
  role,
}: CreateQuery<UserDocument>): Promise<UserDocument> => {
  return UserModel.create({ username, password, role })
    .then((data: UserDocument) => data)
    .catch((error: Error) => {
      throw error;
    });
};

const FindAllUser = async (): Promise<UserDocument[]> => {
  return UserModel.find({})
    .then((data: UserDocument[]) => data)
    .catch((error: Error) => {
      throw error;
    });
};

const FindUserById = async (id: string): Promise<UserDocument | null> => {
  return UserModel.findById(id)
    .then((data: UserDocument | null) => data)
    .catch((error: Error) => {
      throw error;
    });
};

const FindUser = async ({
  username,
  password,
  role,
}: FilterQuery<UserDocument>): Promise<UserDocument | null> => {
  let query = {};
  if (username) query = { ...query, username };
  if (password) query = { ...query, password };
  if (role) query = { ...query, role };
  return UserModel.findOne(query)
    .then((data: UserDocument | null) => data)
    .catch((error: Error) => {
      throw error;
    });
};

const UpdateUser = async (
  id: string,
  { username, password, role }: UpdateQuery<UserDocument>
): Promise<UserDocument | null> => {
  let update = {};
  if (username) update = { ...update, username };
  if (password) update = { ...update, password };
  if (role) update = { ...update, password };

  return UserModel.findByIdAndUpdate(id, update, { new: true });
};

const RemoveUser = async (id: string) => {
  return UserModel.findByIdAndDelete(id);
};

export default {
  CreateUser,
  FindUserById,
  FindUser,
  FindAllUser,
  RemoveUser,
  UpdateUser,
};
