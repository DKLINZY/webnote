import NoteModel, { NoteDocument } from "../models/note.model";
import { CreateQuery, UpdateQuery } from "mongoose";

const CreateNote = async ({
  title,
  content,
  createAt,
  updateAt,
  user,
}: CreateQuery<NoteDocument>): Promise<NoteDocument> => {
  return NoteModel.create({ title, content, createAt, updateAt, user })
    .then((data: NoteDocument) => data)
    .catch((error: Error) => {
      throw error;
    });
};

const FindAllNote = async (): Promise<NoteDocument[]> => {
  return NoteModel.find({})
    .then((data: NoteDocument[]) => data)
    .catch((error: Error) => {
      throw error;
    });
};

const FindNoteById = async (id: string): Promise<NoteDocument | null> => {
  return NoteModel.findById(id)
    .then((data: NoteDocument | null) => data)
    .catch((error: Error) => {
      throw error;
    });
};

const FindNoteByUser = async ({
  username,
  role,
}: {
  username: string;
  role: string;
}): Promise<NoteDocument[]> => {
  return NoteModel.find({ user: { username, role } })
    .then((data: NoteDocument[]) => data)
    .catch((error: Error) => {
      throw error;
    });
};

const UpdateNote = async (
  id: string,
  { title, content, createAt, updateAt, user }: UpdateQuery<NoteDocument>
): Promise<NoteDocument | null> => {
  let update = {};
  if (title) update = { ...update, title };
  if (content) update = { ...update, content };
  if (createAt) update = { ...update, createAt };
  if (updateAt) update = { ...update, updateAt };
  if (user) update = { ...update, user };

  return NoteModel.findByIdAndUpdate(id, update, { new: true });
};

const RemoveNote = async (id: string) => {
  return NoteModel.findByIdAndDelete(id);
};

export default {
  CreateNote,
  FindNoteByUser,
  FindNoteById,
  FindAllNote,
  RemoveNote,
  UpdateNote,
};
