import mongoose, { Error } from "mongoose";
import { DB_URL, DB_USER, DB_PASSWD, ADMIN_PASS } from "./config";
import * as UserService from "./users/users.service";

export default () => {
  const connect = async () => {
    await mongoose
      .connect(DB_URL, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useFindAndModify: false,
        authSource: "admin",
        user: DB_USER,
        pass: DB_PASSWD,
      })
      .then(() => console.info(`Successfully connected to ${DB_URL}`))
      .catch((error) => {
        console.error(`error connecting to database: ${error}`);
        return process.exit(1);
      });
  };
  const createAdminUser = async () => {
    const adminUser = {
      username: "admin",
      password: ADMIN_PASS,
      role: "admin",
    };
    const user = await UserService.find(adminUser);
    if (!user) {
      try {
        await UserService.create(adminUser);
        console.log("create admin user");
      } catch (e) {
        console.log(e.message);
      }
    }
  };
  connect().then(() => {
    createAdminUser();
  });
  mongoose.connection.on("disconnected", connect);
};
