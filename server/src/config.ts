import * as dotenv from "dotenv";

dotenv.config();

const PORT: number = parseInt(process.env.PORT as string, 10);
const DB_HOST: string = process.env.DB_HOST as string;
const DB_PORT: number = parseInt(process.env.DB_PORT as string, 10);
const DB_USER: string = process.env.DB_USER as string;
const DB_PASSWD: string = process.env.DB_PASSWD as string;
const DB_URL = `mongodb://${DB_HOST}:${DB_PORT}/webnote`;
const TOKEN_SECRET: string = process.env.TOKEN_SECRET as string;
const ADMIN_PASS: string = process.env.ADMIN_PASS as string;

export {
  PORT,
  DB_HOST,
  DB_PORT,
  DB_USER,
  DB_PASSWD,
  DB_URL,
  TOKEN_SECRET,
  ADMIN_PASS,
};
