import * as jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";
import { TOKEN_SECRET } from "../config";

export interface DecodeUser {
  username: string;
  role: string;
  iat: number;
}

export const authenticate = (
  request: Request,
  reponse: Response,
  next: NextFunction
) => {
  const authHeader: string | undefined = request.headers.authorization;
  if (authHeader) {
    const token = authHeader.split(" ")[1];
    jwt.verify(token, TOKEN_SECRET, (error: any, user: any) => {
      if (error) return reponse.sendStatus(403);
      // request.user = user;
      next();
    });
  } else {
    reponse.status(401).send("Please login");
  }
};

export const decodeToken = (authorization: string): DecodeUser => {
  const token = authorization.split(" ")[1];
  return jwt.decode(token) as DecodeUser;
};
