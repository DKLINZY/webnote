import { NoteDocument } from "../models/note.model";
import NoteController from "../controller/note.controller";
import { CreateQuery, UpdateQuery } from "mongoose";

const create = async (
  newNote: CreateQuery<NoteDocument>
): Promise<NoteDocument> => {
  return NoteController.CreateNote(newNote);
};

const findAll = async (): Promise<NoteDocument[]> => {
  return NoteController.FindAllNote();
};

const findById = async (id: string): Promise<NoteDocument | null> => {
  return NoteController.FindNoteById(id);
};

const findByUser = async ({
  username,
  role,
}: {
  username: string;
  role: string;
}): Promise<NoteDocument[]> => {
  return NoteController.FindNoteByUser({ username, role });
};

const update = async (
  id: string,
  update: UpdateQuery<NoteDocument>
): Promise<NoteDocument | null> => {
  const note = await NoteController.FindNoteById(id);
  if (!note) {
    return null;
  }
  return NoteController.UpdateNote(id, update);
};

const remove = async (id: string): Promise<NoteDocument | null> => {
  const note = await NoteController.FindNoteById(id);
  if (!note) {
    return null;
  }
  return NoteController.RemoveNote(id);
};

export { findAll, findById, findByUser, create, update, remove };
