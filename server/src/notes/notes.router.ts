import express, { Request, Response } from "express";
import * as NoteService from "./notes.service";
import {
  authenticate,
  decodeToken,
  DecodeUser,
} from "../middleware/auth.middleware";
import { NoteDocument } from "../models/note.model";
import { CreateQuery, UpdateQuery } from "mongoose";

export const noteRouter = express.Router();

noteRouter.get("/", authenticate, async (req: Request, res: Response) => {
  try {
    const decodeUser: DecodeUser = decodeToken(req.headers.authorization as string);
    const notes: NoteDocument[] =
    decodeUser.role === "admin"
        ? await NoteService.findAll()
        : await NoteService.findByUser({
            username: decodeUser.username,
            role: decodeUser.role,
          });
    res.status(200).send(notes);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

noteRouter.get("/:id", authenticate, async (req: Request, res: Response) => {
  try {
    const id: string = req.params.id;
    const note: NoteDocument | null = await NoteService.findById(id);
    if (note) {
      res.status(200).send(note);
    } else {
      res.status(404).send(`Note ${id} Not Found`);
    }
  } catch (e) {
    res.status(500).send(e.message);
  }
});

noteRouter.post("/", authenticate, async (req: Request, res: Response) => {
  try {
    const decodeUser: DecodeUser = decodeToken(req.headers.authorization as string);
    const note: CreateQuery<NoteDocument> = req.body;
    note.user = { username: decodeUser.username, role: decodeUser.role };
    const newNote = await NoteService.create(note);
    res.status(201).json(newNote);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

noteRouter.put("/:id", authenticate, async (req: Request, res: Response) => {
  try {
    const decodeUser: DecodeUser = decodeToken(req.headers.authorization as string);
    const id: string = req.params.id;
    const noteUpdate: UpdateQuery<NoteDocument> = req.body;
    const existingNote: NoteDocument | null = await NoteService.findById(id);
    if (!existingNote) return res.status(404).send(`Note ${id} Not Found`);
    if (
      !(
        existingNote.user.username === decodeUser.username &&
        existingNote.user.role === decodeUser.role
      )
    )
      return res.status(404).send(`Note ${id} Not Found`);
    const updateNote = await NoteService.update(id, noteUpdate);
    res.status(200).json(updateNote);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

noteRouter.delete("/:id", authenticate, async (req: Request, res: Response) => {
  try {
    const decodeUser: DecodeUser = decodeToken(req.headers.authorization as string);
    const id: string = req.params.id;
    const existingNote: NoteDocument | null = await NoteService.findById(id);
    if (!existingNote) return res.status(404).send(`Note ${id} Not Found`);
    if (
      !(
        existingNote.user.username === decodeUser.username &&
        existingNote.user.role === decodeUser.role
      )
    )
      return res.status(404).send(`Note ${id} Not Found`);
    const result: NoteDocument | null = await NoteService.remove(id);
    res.status(200).json(result);
  } catch (e) {
    res.status(500).send(e.message);
  }
});
