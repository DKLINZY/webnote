import express, { Request, Response } from "express";
import * as UserService from "./users.service";
import {
  authenticate,
  decodeToken,
  DecodeUser,
} from "../middleware/auth.middleware";
import { UserDocument } from "../models/user.model";
import { CreateQuery, UpdateQuery } from "mongoose";

export const userRouter = express.Router();

userRouter.get("/", authenticate, async (req: Request, res: Response) => {
  try {
    const decodeUser: DecodeUser = decodeToken(
      req.headers.authorization as string
    );
    if (decodeUser.role !== "admin")
      return res.status(403).send("Please Login as admin");
    const users: UserDocument[] = await UserService.findAll();
    res.status(200).send(users);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

userRouter.get("/:id", authenticate, async (req: Request, res: Response) => {
  try {
    const decodeUser: DecodeUser = decodeToken(
      req.headers.authorization as string
    );
    if (decodeUser.role !== "admin")
      return res.status(403).send("Please Login as admin");
    const id: string = req.params.id;
    const user: UserDocument | null = await UserService.findById(id);
    if (user) {
      res.status(200).send(user);
    } else {
      res.status(404).send(`User ${id} Not Found`);
    }
  } catch (e) {
    res.status(500).send(e.message);
  }
});

userRouter.post("/", authenticate, async (req: Request, res: Response) => {
  try {
    const decodeUser: DecodeUser = decodeToken(
      req.headers.authorization as string
    );
    if (decodeUser.role !== "admin")
      return res.status(403).send("Please Login as admin");
    const user: CreateQuery<UserDocument> = req.body;
    const newUser = await UserService.create(user);
    res.status(201).json(newUser);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

userRouter.put("/:id", authenticate, async (req: Request, res: Response) => {
  try {
    const decodeUser: DecodeUser = decodeToken(
      req.headers.authorization as string
    );
    // if (decodeUser.role !== "admin")
    //   return res.status(403).send("Please Login as admin");
    const id: string = req.params.id;
    const userUpdate: UpdateQuery<UserDocument> = req.body;
    const existingUser: UserDocument | null = await UserService.findById(id);

    if (existingUser) {
      const updateUser = await UserService.update(id, userUpdate);
      res.status(200).json(updateUser);
    } else {
      res.status(404).send(`User ${id} Not Found`);
    }
  } catch (e) {
    res.status(500).send(e.message);
  }
});

userRouter.delete("/:id", authenticate, async (req: Request, res: Response) => {
  try {
    const decodeUser: DecodeUser = decodeToken(
      req.headers.authorization as string
    );
    if (decodeUser.role !== "admin")
      return res.status(403).send("Please Login as admin");
    const id: string = req.params.id;
    const existingUser: UserDocument | null = await UserService.findById(id);
    if (existingUser) {
      const result: UserDocument | null = await UserService.remove(id);
      res.status(200).json(result);
    } else {
      res.status(404).send(`User ${id} Not Found`);
    }
  } catch (e) {
    res.status(500).send(e.message);
  }
});
