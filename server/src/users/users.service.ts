import { UserDocument } from "../models/user.model";
import UserController from "../controller/user.controller";
import { CreateQuery, FilterQuery, UpdateQuery } from "mongoose";

const create = async (
  newUser: CreateQuery<UserDocument>
): Promise<UserDocument> => {
  return UserController.CreateUser(newUser);
};

const findAll = async (): Promise<UserDocument[]> => {
  return UserController.FindAllUser();
};

const findById = async (id: string): Promise<UserDocument | null> => {
  return UserController.FindUserById(id);
};

const find = async (
  user: FilterQuery<UserDocument>
): Promise<UserDocument | null> => {
  return UserController.FindUser(user);
};

const update = async (
  id: string,
  update: UpdateQuery<UserDocument>
): Promise<UserDocument | null> => {
  const user = await UserController.FindUserById(id);
  if (!user) {
    return null;
  }
  return UserController.UpdateUser(id, update);
};

const remove = async (id: string): Promise<UserDocument | null> => {
  const user = await UserController.FindUserById(id);
  if (!user) {
    return null;
  }
  return UserController.RemoveUser(id);
};

export { findAll, find, findById, create, update, remove };
