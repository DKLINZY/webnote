/**
 * Required External Modules
 */
import express from "express";
import cors from "cors";
import helmet from "helmet";

import { noteRouter } from "./notes/notes.router";
import { userRouter } from "./users/users.router";
import { loginRouter } from "./login/login.router";

import { errorHandle } from "./middleware/error.middleware";
import { notFoundHandler } from "./middleware/not-found.middleware";
import connect from "./connect";
import { PORT, DB_URL, DB_USER, DB_PASSWD } from "./config";

/**
 * App Variables
 */
const app = express();

if (!PORT) {
  process.exit(1);
}
/**
 *  App Configuration
 */
app.use(helmet());
app.use(cors());
app.use(express.json());
app.use("/api/notes", noteRouter);
app.use("/api/users", userRouter);
app.use("/api/login", loginRouter);

app.use(errorHandle);
app.use(notFoundHandler);

/**
 * Server Activation
 */
app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`);
});
connect();
