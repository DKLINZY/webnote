const getBearerToken = (token: string): string => {
  return `Bearer ${token}`;
};

export { getBearerToken };
