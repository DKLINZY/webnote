import { BaseUser, UserAuth } from "../interface/User";
import { LoginUrl } from "./apiUrl";
import { getBearerToken } from "./BearerToken";

class LoginApi {
  login = async ({
    username,
    password,
    role,
  }: BaseUser): Promise<UserAuth | null> => {
    const response = await fetch(LoginUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ username, password }),
    });
    const responseJson = await response.json();
    // console.log(response)
    if ("userAuth" in responseJson) return responseJson.userAuth as UserAuth;
    return null;
  };

  checkLogin = async (token: string): Promise<UserAuth | null> => {
    const response = await fetch(LoginUrl, {
      method: "GET",
      headers: {
        Authorization: getBearerToken(token),
      },
    });
    const responseJson = await response.json();
    if ("userAuth" in responseJson) return responseJson.userAuth as UserAuth;
    return null;
  };
}

export default new LoginApi();
