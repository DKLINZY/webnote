import { Note, BaseNote } from "../interface/Note";
import { NoteUrl } from "./apiUrl";
import { getBearerToken } from "./BearerToken";

class NoteApi {
  getNote = async (token: string): Promise<Note[] | null> => {
    try {
      const response = await fetch(NoteUrl, {
        method: "GET",
        headers: {
          Authorization: getBearerToken(token),
        },
      });
      const responseJson = await response.json();
      if (response.ok) {
        return responseJson as Note[];
      } else {
        throw new Error("Network Error");
      }
    } catch (e) {
      console.log(e.message);
      return null;
    }
  };

  addNote = async (token: string, newNote: BaseNote): Promise<Note | null> => {
    try {
      const response = await fetch(NoteUrl, {
        method: "POST",
        headers: {
          Authorization: getBearerToken(token),
          "Content-Type": "application/json",
        },
        body: JSON.stringify(newNote),
      });
      const responseJson = await response.json();
      if (response.ok) {
        return responseJson as Note;
      } else {
        throw new Error("Network Error");
      }
    } catch (e) {
      console.log(e.message);
      return null;
    }
  };

  updateNote = async (
    token: string,
    noteId: string,
    updatedNote: BaseNote
  ): Promise<Note | null> => {
    try {
      const response = await fetch(`${NoteUrl}/${noteId}`, {
        method: "PUT",
        headers: {
          Authorization: getBearerToken(token),
          "Content-Type": "application/json",
        },
        body: JSON.stringify(updatedNote),
      });
      const responseJson = await response.json();
      if (response.ok) {
        return responseJson as Note;
      } else {
        throw new Error("Network Error");
      }
    } catch (e) {
      console.log(e.message);
      return null;
    }
  };

  deleteNote = async (token: string, noteId: string): Promise<Note | null> => {
    try {
      const response = await fetch(`${NoteUrl}/${noteId}`, {
        method: "DELETE",
        headers: {
          Authorization: getBearerToken(token),
        },
      });
      const responseJson = await response.json();
      if (response.ok) {
        return responseJson as Note;
      } else {
        throw new Error("Network Error");
      }
    } catch (e) {
      console.log(e.message);
      return null;
    }
  };
}

export default new NoteApi();
