import { BaseUser } from "../interface/User";
import { LoginUrl } from "./apiUrl";

class AuthApi {
  login = async ({
    username,
    password,
    role,
  }: BaseUser): Promise<string | null> => {
    const response = await fetch(LoginUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ username, password }),
    });
    const responseJson = await response.json();
    // console.log(response)
    if ("token" in responseJson) return responseJson.token as string;
    return null;
  };
}

export default new AuthApi();
