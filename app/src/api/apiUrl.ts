const RootUrl = "http://localhost:7000/api";
const LoginUrl = `${RootUrl}/login`;
const NoteUrl = `${RootUrl}/notes`;
const UserUrl = `${RootUrl}/users`;

export { LoginUrl, NoteUrl, UserUrl };
