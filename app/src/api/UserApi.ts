import { UserUrl } from "./apiUrl";
import { getBearerToken } from "./BearerToken";

import { BaseUser, User } from "../interface/User";

class UserApi {
  getUser = async (token: string): Promise<User[] | null> => {
    try {
      const response = await fetch(UserUrl, {
        method: "GET",
        headers: {
          Authorization: getBearerToken(token),
        },
      });
      const responseJson = await response.json();

      if (response.ok) {
        return responseJson as User[];
      } else {
        throw new Error("Network Error");
      }
    } catch (e) {
      console.log(e.message);
      return null;
    }
  };

  addUser = async (token: string, newUser: BaseUser): Promise<User | null> => {
    try {
      const response = await fetch(UserUrl, {
        method: "POST",
        headers: {
          Authorization: getBearerToken(token),
          "Content-Type": "application/json",
        },
        body: JSON.stringify(newUser),
      });
      const responseJson = await response.json();
      if (response.ok) {
        return responseJson as User;
      } else {
        throw new Error("Network Error");
      }
    } catch (e) {
      console.log(e.message);
      return null;
    }
  };

  updateUser = async (
    token: string,
    userId: string,
    updatedUser: BaseUser
  ): Promise<User | null> => {
    try {
      const response = await fetch(`${UserUrl}/${userId}`, {
        method: "PUT",
        headers: {
          Authorization: getBearerToken(token),
          "Content-Type": "application/json",
        },
        body: JSON.stringify(updatedUser),
      });
      const responseJson = await response.json();
      if (response.ok) {
        return responseJson as User;
      } else {
        throw new Error("Network Error");
      }
    } catch (e) {
      console.log(e.message);
      return null;
    }
  };

  deleteUser = async (token: string, userId: string): Promise<User | null> => {
    try {
      const response = await fetch(`${UserUrl}/${userId}`, {
        method: "DELETE",
        headers: {
          Authorization: getBearerToken(token),
        },
      });
      const responseJson = await response.json();
      if (response.ok) {
        return responseJson as User;
      } else {
        throw new Error("Network Error");
      }
    } catch (e) {
      console.log(e.message);
      return null;
    }
  };
}

export default new UserApi();
