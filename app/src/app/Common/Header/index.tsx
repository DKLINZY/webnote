import React from "react";
import { useHistory } from "react-router-dom";
import { FormControl, DropdownButton, Dropdown, Button } from "react-bootstrap";
import { observer } from "mobx-react";

import { useStore } from "../../../store";

type HeaderProps = {
  routeName: string;
};
const Header = ({ routeName }: HeaderProps) => {
  const {
    noteStore: { keyword, updateKeyword, newNote },
    loginStore: {
      logout,
      currentUser: { username },
    },
  } = useStore();
  const history = useHistory();

  return (
    <div
      className="d-flex align-items-center justify-content-between mx-3"
      style={{ width: "100%" }}
    >
      <div className="text-left" style={{ width: "10rem" }}>
        <span
          className="header-text"
          style={{
            fontSize: 25,
            fontWeight: "bold",
            width: "100%",
            cursor: "pointer",
            userSelect: "none",
          }}
          onClick={() => history.push("/login")}
        >
          Web Note
        </span>
      </div>

      <div className="d-flex justify-content-end">
        {routeName === "login" ? (
          <></>
        ) : (
          <>
            {routeName === "account" ? (
              <></>
            ) : (
              <>
                <div className="px-1">
                  <FormControl
                    placeholder="Search..."
                    value={keyword}
                    onChange={(e) => updateKeyword(e.target.value)}
                  />
                </div>
                <div className="px-1">
                  <Button
                    variant="light"
                    style={{ width: "5rem" }}
                    onClick={newNote}
                  >
                    New
                  </Button>
                </div>
              </>
            )}

            <div className="px-1">
              <DropdownButton
                variant="light"
                menuAlign="right"
                title={username}
              >
                <Dropdown.Item onClick={() => history.push("/account")}>
                  Account
                </Dropdown.Item>
                <Dropdown.Item onClick={logout}>Logout</Dropdown.Item>
              </DropdownButton>
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default observer(Header);
