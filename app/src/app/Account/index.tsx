import React from "react";
import { Container, Row } from "react-bootstrap";

import Header from "../Common/Header";
import TabForm from "./TabForm";

type AccountPorps = {
  windowHeight: number;
  headerHeight: number;
  hearderMarinBottom: number;
};

const Account = ({
  windowHeight,
  headerHeight,
  hearderMarinBottom,
}: AccountPorps) => {
  return (
    <Container fluid>
      <Row
        style={{
          height: headerHeight,
          boxShadow:
            "0 0.5rem 1rem rgb(0 0 0 / 5%), inset 0 -1px 0 rgb(0 0 0 / 15%)",
          marginBottom: hearderMarinBottom,
        }}
      >
        <Header routeName={"account"} />
      </Row>
      <Row className="d-flex justify-content-center">
        <TabForm />
      </Row>
    </Container>
  );
};

export default Account;
