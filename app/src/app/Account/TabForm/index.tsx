import React, { useState } from "react";
import { observer } from "mobx-react";
import { Tab, Tabs } from "react-bootstrap";

import UpdatePassword from "./UpdatePassword";
import UserManage from "./UserManage";

import { useStore } from "../../../store";

const TabForm = () => {
  const {
    loginStore: { currentUser },
  } = useStore();
  const [key, setKey] = useState<string>("update_password");

  return (
    <div
      style={{
        minWidth: "40rem",
        width: "60rem",
        margin: "1rem 2rem",
        padding: "1.5rem",
        borderRadius: "0.75rem",
        border: "2px solid rgb(236, 236, 236)",
      }}
    >
      <Tabs
        id="controlled-tab-example"
        activeKey={key}
        onSelect={(eventKey) => {
          if (eventKey) setKey(eventKey);
        }}
      >
        <Tab eventKey="update_password" title="Update Password">
          <UpdatePassword />
        </Tab>
        {currentUser.role === "admin" ? (
          <Tab eventKey="user_manage" title="User Management">
            <UserManage />
          </Tab>
        ) : (
          ""
        )}
      </Tabs>
    </div>
  );
};

export default observer(TabForm);
