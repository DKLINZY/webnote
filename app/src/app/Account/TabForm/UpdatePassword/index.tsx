import React, { useState, MouseEvent } from "react";
import { observer } from "mobx-react";
import { Button, Form, Alert } from "react-bootstrap";

import { useStore } from "../../../../store";

const UpdatePassword = () => {
  const {
    accountStore: {
      updatePassword,
      alertMessage,
      closeAlertMessage,
      updatePasswordCheck,
      isLoading,
    },
  } = useStore();

  const [newPassword, setNewPassword] = useState<string>("");
  const [confirmNewPassword, setConfirmNewPassword] = useState<string>("");

  return (
    <div>
      <div
        className="d-flex justify-content-center"
        style={{
          padding: "1.5rem",
        }}
      >
        <Form
          style={{
            width: "25rem",
          }}
        >
          <Form.Group controlId="new_password">
            <Form.Label>New Password</Form.Label>
            <Form.Control
              isInvalid={updatePasswordCheck.newPassword.isInvalid}
              value={newPassword}
              type="password"
              placeholder="New Password"
              onChange={(e) => {
                setNewPassword(e.target.value);
              }}
            />
            <Form.Control.Feedback type="invalid">
              {updatePasswordCheck.newPassword.feedback}
            </Form.Control.Feedback>
          </Form.Group>

          <Form.Group controlId="confirm_new_password">
            <Form.Label>Confirm New Password</Form.Label>
            <Form.Control
              isInvalid={updatePasswordCheck.confirmNewPassword.isInvalid}
              value={confirmNewPassword}
              type="password"
              placeholder="Confirm New Password"
              onChange={(e) => {
                setConfirmNewPassword(e.target.value);
              }}
            />
            <Form.Control.Feedback type="invalid">
              {updatePasswordCheck.confirmNewPassword.feedback}
            </Form.Control.Feedback>
          </Form.Group>
          <div className="text-center mt-4">
            <Button
              variant="primary"
              type="submit"
              style={{ width: "12rem", height: "2.5rem" }}
              onClick={(e: MouseEvent) => {
                updatePassword(e, {
                  newPassword,
                  confirmNewPassword,
                });
              }}
              disabled={isLoading}
            >
              {isLoading ? "Loading..." : "Update"}
            </Button>
          </div>
        </Form>
      </div>
      <div>
        {alertMessage.isShow ? (
          <Alert
            style={{ width: "30rem", margin: "auto" }}
            className="text-center"
            variant={alertMessage.variant}
            onClose={closeAlertMessage}
            dismissible
          >
            {alertMessage.message}
          </Alert>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};

export default observer(UpdatePassword);
