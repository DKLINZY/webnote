import React from "react";
import { observer } from "mobx-react";
import { ListGroup } from "react-bootstrap";

import { useStore } from "../../../../store";

const UserList = () => {
  const {
    accountStore: { users, selectedUserIndex, selectUser },
  } = useStore();

  return (
    <ListGroup variant="flush">
      {users.map((user, index) => (
        <ListGroup.Item
          key={index}
          action
          active={index === selectedUserIndex}
          onClick={() => selectUser(index)}
        >
          <div className="text-truncate">{user.username}</div>
        </ListGroup.Item>
      ))}
    </ListGroup>
  );
};

export default observer(UserList);
