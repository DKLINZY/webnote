import React from "react";
import { Container, Row, Col } from "react-bootstrap";

import UserList from "./UserList";
import UserDetail from "./UserDetail";

const UserManage = () => {
  return (
    <Container fluid style={{ padding: "2rem" }}>
      <Row>
        <Col
          xs={4}
          xl={3}
          style={{
            overflowY: "auto",
            height: "25rem",
          }}
        >
          <UserList />
        </Col>
        <Col>
          <UserDetail />
        </Col>
      </Row>
    </Container>
  );
};
export default UserManage;
