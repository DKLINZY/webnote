import React, { useEffect, useState } from "react";
import { observer } from "mobx-react";
import { Form, Button, Alert } from "react-bootstrap";
import {
  FileEarmarkPlusFill,
  FileEarmarkPostFill,
  TrashFill,
} from "react-bootstrap-icons";

import { useStore } from "../../../../store";
import { User } from "../../../../interface/User";

const UserDetail = () => {
  const {
    accountStore: {
      users,
      selectedUserIndex,
      userManagementCheck,
      alertMessage,
      closeAlertMessage,
      addUser,
      updateUser,
      deleteUser,
    },
  } = useStore();
  const [selectedUser, setSelectUser] = useState<User>({
    _id: "",
    username: "",
    password: "",
    role: "",
  });

  const newUser = () => {
    setSelectUser({
      _id: "",
      username: "",
      password: "",
      role: "user",
    });
  };

  const saveUser = () => {
    if (selectedUser._id === "") {
      addUser(selectedUser);
    } else {
      updateUser(selectedUser);
    }
  };

  useEffect(() => {
    if (users.length > 0) {
      setSelectUser({
        _id: users[selectedUserIndex]._id,
        username: users[selectedUserIndex].username,
        password: users[selectedUserIndex].password,
        role: users[selectedUserIndex].role,
      });
    } else {
      setSelectUser({
        _id: "",
        username: "",
        password: "",
        role: "",
      });
    }
  }, [users, selectedUserIndex]);

  return (
    <div className="d-flex flex-column justify-content-center">
      <Form
        style={{
          width: "100%",
          paddingLeft: "4rem",
          paddingRight: "4rem",
          paddingBottom: "2rem",
        }}
      >
        <Form.Group controlId="username">
          <Form.Label>Username</Form.Label>
          <Form.Control
            isInvalid={userManagementCheck.username.isInvalid}
            value={selectedUser.username}
            type="text"
            placeholder="Username"
            onChange={(e) => {
              setSelectUser({
                ...selectedUser,
                username: e.target.value.trim(),
              });
            }}
          />
          <Form.Control.Feedback type="invalid">
            {userManagementCheck.username.feedback}
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label>Password</Form.Label>
          <Form.Control
            isInvalid={userManagementCheck.password.isInvalid}
            value={selectedUser.password}
            type="password"
            placeholder="Password"
            onChange={(e) => {
              setSelectUser({
                ...selectedUser,
                password: e.target.value.trim(),
              });
            }}
          />
          <Form.Control.Feedback type="invalid">
            {userManagementCheck.password.feedback}
          </Form.Control.Feedback>
        </Form.Group>

        <Form.Check
          type="checkbox"
          id={`default-checkbox`}
          label={`Admin User`}
          checked={selectedUser.role === "admin" ? true : false}
          onChange={() => {
            setSelectUser({
              ...selectedUser,
              role: selectedUser.role === "admin" ? "user" : "admin",
            });
          }}
        />

        <div className="d-flex justify-content-end align-self-end mt-4">
          <div className="px-1">
            <Button variant="light" style={{ width: "4rem" }} onClick={newUser}>
              <FileEarmarkPlusFill />
            </Button>
          </div>
          <div className="px-1">
            <Button
              variant="light"
              style={{ width: "4rem" }}
              onClick={saveUser}
            >
              <FileEarmarkPostFill />
            </Button>
          </div>
          <div className="px-1">
            <Button
              variant="light"
              style={{ width: "rem" }}
              onClick={() => {
                deleteUser(selectedUser._id);
              }}
            >
              <TrashFill />
            </Button>
          </div>
        </div>
      </Form>
      <div>
        {alertMessage.isShow ? (
          <Alert
            style={{ width: "30rem", margin: "auto" }}
            className="text-center"
            variant={alertMessage.variant}
            onClose={closeAlertMessage}
            dismissible
          >
            {alertMessage.message}
          </Alert>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};

export default observer(UserDetail);
