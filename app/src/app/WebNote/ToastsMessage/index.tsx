import React from "react";
import { Toast } from "react-bootstrap";
import { observer } from "mobx-react";

import { useStore } from "../../../store";

type ToastsMessageProps = {
  marginTop: number;
};

const ToastsMessage = ({ marginTop }: ToastsMessageProps) => {
  const {
    noteStore: { toastsMessage, closeToastsMessage },
  } = useStore();
  return (
    <div
      className="position-fixed"
      style={{
        width: 350,
        zIndex: 1100,
        marginTop: marginTop,
        right: 0,
      }}
    >
      <Toast
        style={{ margin: 15 }}
        onClose={closeToastsMessage}
        show={toastsMessage.show}
        delay={3000}
        autohide
      >
        <Toast.Header>
          <strong className="mx-auto">{toastsMessage.title}</strong>
        </Toast.Header>
        <Toast.Body>{toastsMessage.message}</Toast.Body>
      </Toast>
    </div>
  );
};

export default observer(ToastsMessage);
