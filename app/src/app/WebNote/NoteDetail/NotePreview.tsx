import React from "react";
import { observer } from "mobx-react";
import ReactMarkdown from "react-markdown";
import gfm from "remark-gfm";

import { useStore } from "../../../store";

const NotePreview = () => {
  const {
    noteStore: { currentNote },
  } = useStore();

  return (
    <div
      className="d-flex-column p-2 bg-light border rounded"
      style={{ width: "100%" }}
    >
      <ReactMarkdown
        plugins={[gfm]}
        children={currentNote.content ? currentNote.content : ""}
      />
    </div>
  );
};

export default observer(NotePreview);
