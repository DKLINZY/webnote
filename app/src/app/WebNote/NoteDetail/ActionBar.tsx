import React from "react";
import { observer } from "mobx-react";
import { FormControl, Button } from "react-bootstrap";
import { FileEarmarkPostFill, TrashFill } from "react-bootstrap-icons";

import { useStore } from "../../../store";

const ActionBar = () => {
  const {
    noteStore: { currentNote, setCurrentNote, saveNote, deleteNote },
  } = useStore();
  return (
    <div className="d-flex" style={{ width: "100%" }}>
      <FormControl
        placeholder="title"
        className="mr-2"
        value={currentNote.title}
        onChange={(e) => {
          setCurrentNote({ title: e.target.value });
        }}
      />
      <div className="px-1">
        <Button variant="light" style={{ width: "3rem" }} onClick={saveNote}>
          <FileEarmarkPostFill />
        </Button>
      </div>
      <div className="px-1">
        <Button variant="light" style={{ width: "3rem" }} onClick={deleteNote}>
          <TrashFill />
        </Button>
      </div>
    </div>
  );
};

export default observer(ActionBar);
