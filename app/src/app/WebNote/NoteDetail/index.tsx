import React from "react";
import { observer } from "mobx-react";
import { Container, Row } from "react-bootstrap";

import ActionBar from "./ActionBar";
import NoteEditor from "./NoteEditor";
import NotePreview from "./NotePreview";

type NoteDetailProps = {
  containerHeight: number;
  titleHeight: number;
};

const NoteDetail = ({ containerHeight, titleHeight }: NoteDetailProps) => {
  const marginButtom: number = 5;
  const newContainerHeight: number = containerHeight - marginButtom;

  return (
    <Container fluid>
      <Row style={{ height: titleHeight }}>
        <ActionBar />
      </Row>
      <Row
        style={{
          height: newContainerHeight,
          marginBottom: marginButtom,
          overflowY: "scroll",
        }}
      >
        <NotePreview />
      </Row>
      <Row
        style={{
          height: newContainerHeight,
          marginBottom: marginButtom,
        }}
      >
        <NoteEditor />
      </Row>
    </Container>
  );
};

export default observer(NoteDetail);
