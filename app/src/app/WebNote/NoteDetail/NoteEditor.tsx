import React from "react";
import { observer } from "mobx-react";

import { useStore } from "../../../store";

const NoteEditor = () => {
  const {
    noteStore: { currentNote, setCurrentNote },
  } = useStore();

  return (
    <div className="form-floating" style={{ width: "100%" }}>
      <textarea
        className="form-control"
        style={{ height: "100%", resize: "none" }}
        value={currentNote.content ? currentNote.content : ""}
        onChange={(e) => setCurrentNote({ content: e.target.value })}
      ></textarea>
    </div>
  );
};

export default observer(NoteEditor);
