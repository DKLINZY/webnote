import React from "react";
import { Container, Col, Row } from "react-bootstrap";

import Header from "../Common/Header";
import NoteList from "./NoteList";
import NoteDetail from "./NoteDetail";
import ToastsMessage from "./ToastsMessage";

type WebNoteProps = {
  windowHeight: number;
  headerHeight: number;
  hearderMarinBottom: number;
};

const WebNote = ({
  windowHeight,
  headerHeight,
  hearderMarinBottom,
}: WebNoteProps) => {
  const titleHeight: number = 50;

  return (
    <>
      <ToastsMessage marginTop={headerHeight} />
      <Container fluid>
        <Row
          style={{
            height: headerHeight,
            boxShadow:
              "0 0.5rem 1rem rgb(0 0 0 / 5%), inset 0 -1px 0 rgb(0 0 0 / 15%)",
            marginBottom: hearderMarinBottom,
          }}
        >
          <Header routeName={"webnote"} />
        </Row>
        <Row>
          <Col
            xs={4}
            xl={3}
            style={{
              height: windowHeight - headerHeight - hearderMarinBottom,
              overflowY: "scroll",
            }}
          >
            <NoteList />
          </Col>
          <Col
            style={{ height: windowHeight - headerHeight - hearderMarinBottom }}
          >
            <NoteDetail
              titleHeight={titleHeight}
              containerHeight={
                (windowHeight -
                  headerHeight -
                  hearderMarinBottom -
                  titleHeight) /
                2
              }
            />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default WebNote;
