import React, { useEffect, useCallback } from "react";
import moment from "moment";
import { ListGroup, Col, Row } from "react-bootstrap";
import { observer } from "mobx-react";

import { useStore } from "../../../store";

const NoteList = () => {
  const {
    noteStore: { notes, selectedNoteIndex, fetchNote, selectNote },
  } = useStore();

  const fetchNoteList = useCallback(fetchNote, [fetchNote]);

  useEffect(() => {
    fetchNoteList();
  }, [fetchNoteList]);

  return (
    <ListGroup variant="flush">
      {notes.map((note, index) => (
        <ListGroup.Item
          key={index}
          action
          active={index === selectedNoteIndex}
          onClick={() => selectNote(index)}
        >
          <Row>
            <Col className="h5 font-weight-bold ">{note.title}</Col>
            <Col className="text-right text-justify">
              {moment(note.createAt).format("DD MMM YYYY")}
              {/* {note.createAt.toDateString()} */}
            </Col>
          </Row>
          <div className="text-truncate">{note.content}</div>
        </ListGroup.Item>
      ))}
    </ListGroup>
  );
};

export default observer(NoteList);
