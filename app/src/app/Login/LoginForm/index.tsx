import React, { useState, MouseEvent } from "react";
import { Button, Form } from "react-bootstrap";
import { observer } from "mobx-react";

import { useStore } from "../../../store";

const LoginForm = () => {
  const {
    loginStore: { login, isLoading, inputCheck },
  } = useStore();

  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  return (
    <div
      className="d-flex flex-column justify-content-center align-items-center"
      style={{ height: "100%", width: "100%" }}
    >
      <div
        style={{
          width: "24rem",
          backgroundColor: "white",
          borderRadius: 10,
          boxShadow: "0rem 0rem 1rem 0.6rem rgb(0 0 0 / 5%)",
          padding: 30,
        }}
      >
        <Form>
          <Form.Group controlId="username">
            <Form.Label>Username</Form.Label>
            <Form.Control
              isInvalid={inputCheck.username.isInvalid}
              value={username}
              placeholder="Enter Username"
              onChange={(e) => {
                setUsername(e.target.value);
              }}
            />
            <Form.Control.Feedback type="invalid">
              {inputCheck.username.feedback}
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              isInvalid={inputCheck.password.isInvalid}
              value={password}
              type="password"
              placeholder="Password"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
            />
            <Form.Control.Feedback type="invalid">
              {inputCheck.password.feedback}
            </Form.Control.Feedback>
          </Form.Group>
          <div className="text-center mt-4">
            <Button
              variant="primary"
              type="submit"
              style={{ width: "12rem", height: "2.5rem" }}
              onClick={(e: MouseEvent) => {
                login(e, {
                  username,
                  password,
                  role: "",
                });
              }}
              disabled={isLoading}
            >
              {isLoading ? "Loading..." : "Login"}
            </Button>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default observer(LoginForm);
