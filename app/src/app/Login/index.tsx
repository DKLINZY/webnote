import React from "react";
import { Container, Row } from "react-bootstrap";

import Header from "../Common/Header";
import LoginForm from "./LoginForm";

type LoginPorps = {
  windowHeight: number;
  headerHeight: number;
  hearderMarinBottom: number;
};

const Login = ({
  windowHeight,
  headerHeight,
  hearderMarinBottom,
}: LoginPorps) => {
  return (
    <Container fluid>
      <Row
        style={{
          height: headerHeight,
          boxShadow:
            "0 0.5rem 1rem rgb(0 0 0 / 5%), inset 0 -1px 0 rgb(0 0 0 / 15%)",
          marginBottom: hearderMarinBottom,
        }}
      >
        <Header routeName={"login"} />
      </Row>
      <Row
        style={{
          height: windowHeight - headerHeight - hearderMarinBottom,
        }}
      >
        <LoginForm />
      </Row>
    </Container>
  );
};

export default Login;
