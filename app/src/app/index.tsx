import React, { useState, useEffect } from "react";
import { observer } from "mobx-react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import Login from "./Login";
import WebNote from "./WebNote";
import Account from "./Account";

import { useStore } from "../store";

const App = () => {
  const {
    loginStore: {
      currentUser: { isLogged },
    },
  } = useStore();
  const [windowHeight, setWindowHeight] = useState<number>(window.innerHeight);
  const headerHeight: number = 60;
  const hearderMarinBottom: number = 20;

  useEffect(() => {
    window.addEventListener("resize", () => {
      setWindowHeight(window.innerHeight);
    });
  }, []);

  return (
    <Router>
      <Switch>
        <Route path="/login">
          {isLogged ? (
            <Redirect to="/" />
          ) : (
            <Login
              windowHeight={windowHeight}
              headerHeight={headerHeight}
              hearderMarinBottom={hearderMarinBottom}
            />
          )}
        </Route>
        <Route path="/account">
          {isLogged ? (
            <Account
              windowHeight={windowHeight}
              headerHeight={headerHeight}
              hearderMarinBottom={hearderMarinBottom}
            />
          ) : (
            <Redirect to="/login" />
          )}
        </Route>
        <Route path="/">
          {isLogged ? (
            <WebNote
              windowHeight={windowHeight}
              headerHeight={headerHeight}
              hearderMarinBottom={hearderMarinBottom}
            />
          ) : (
            <Redirect to="/login" />
          )}
        </Route>
      </Switch>
    </Router>
  );
};

export default observer(App);
