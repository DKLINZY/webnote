import { makeAutoObservable, runInAction } from "mobx";

import { Note, BaseNote } from "../interface/Note";
import { CurrentNoteProps } from "../interface/CurrentNoteProps";
import { ToastsMessage, ToastsMessageProps } from "../interface/ToastsMessage";
import { NoteApi } from "../api";
import { RootStore } from ".";

class NoteStore {
  rootStore: RootStore;
  private _noteList: Note[] = [];
  private _keyword: string = "";
  private _selectedNoteIndex: number = -1;
  private _emptyNote: Note = {
    _id: "",
    title: "",
    content: "",
    createAt: new Date(1970, 1, 1),
  };
  private _currentNote: Note = { ...this.emptyNote };
  private _toastsMessage: ToastsMessage = {
    show: false,
    title: "",
    message: "",
  };

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
    makeAutoObservable(this);
  }

  get noteList(): Note[] {
    return this._noteList;
  }

  set noteList(value: Note[]) {
    this._noteList = value;
  }

  get keyword(): string {
    return this._keyword;
  }

  set keyword(value: string) {
    this._keyword = value;
  }

  get selectedNoteIndex(): number {
    return this._selectedNoteIndex;
  }

  set selectedNoteIndex(value: number) {
    this._selectedNoteIndex = value;
  }

  get emptyNote(): Note {
    return this._emptyNote;
  }

  set emptyNote(value: Note) {
    this._emptyNote = value;
  }

  get currentNote(): Note {
    return this._currentNote;
  }

  set currentNote(value: Note) {
    this._currentNote = value;
  }

  get toastsMessage(): ToastsMessage {
    return this._toastsMessage;
  }

  set ToastsMessage(value: ToastsMessage) {
    this._toastsMessage = value;
  }

  get notes(): Note[] {
    if (this.keyword === "") return this.noteList;
    else
      return this.noteList.filter(
        (note) =>
          note.title
            .toLowerCase()
            .includes(this.keyword.trim().toLowerCase()) ||
          note.content
            ?.toLowerCase()
            .includes(this.keyword.trim().toLowerCase())
      );
  }

  newNote = () => {
    this.currentNote._id = this.emptyNote._id;
    this.currentNote.title = this.emptyNote.title;
    this.currentNote.content = this.emptyNote.content;
    this.currentNote.createAt = this.emptyNote.createAt;
    this.currentNote.updateAt = this.emptyNote.updateAt;
    this.selectedNoteIndex = -1;
  };

  fetchNote = async () => {
    const notes = await NoteApi.getNote(
      this.rootStore.loginStore.currentUser.token
    );
    if (notes) {
      this.noteList = notes;
    } else {
      this.updateToastsMessage({
        title: `Fetch Note`,
        message: `Failed to Fetch Note`,
      });
    }
    // console.log(notes);
  };

  saveNote = async () => {
    if (this.currentNote.title.length === 0) {
      this.updateToastsMessage({
        title: `Save Note`,
        message: `Note Title Can't be EMPTY`,
      });
      return;
    }
    if (this.currentNote._id.length > 0) {
      // update
      const updatedNote: BaseNote = {
        title: this.currentNote.title,
        content: this.currentNote.content ? this.currentNote.content : "",
        createAt: this.currentNote.createAt,
        updateAt: new Date(),
      };
      const note = await NoteApi.updateNote(
        this.rootStore.loginStore.currentUser.token,
        this.currentNote._id,
        updatedNote
      );
      if (note) {
        const index = this.noteList.findIndex(
          (note) => this.currentNote._id === note._id
        );
        runInAction(() => {
          this.noteList[index] = { ...this.currentNote };
        });
      }
    } else {
      // new
      const newNote: BaseNote = {
        title: this.currentNote.title,
        content: this.currentNote.content ? this.currentNote.content : "",
        createAt: new Date(),
      };

      const note = await NoteApi.addNote(
        this.rootStore.loginStore.currentUser.token,
        newNote
      );

      if (note) {
        runInAction(() => {
          this.noteList.push(note);
          this.currentNote._id = note._id;
        });
        this.keyword = "";
        this.selectedNoteIndex = this.notes.findIndex(
          (note) => this.currentNote._id === note._id
        );
      }
    }
    this.updateToastsMessage({
      title: `Save Note`,
      message: `${this.currentNote.title} saved`,
    });
  };

  deleteNote = async () => {
    if (this.currentNote._id.length) {
      const note = await NoteApi.deleteNote(
        this.rootStore.loginStore.currentUser.token,
        this.currentNote._id
      );
      if (note) {
        const index = this.noteList.findIndex(
          (note) => this.currentNote._id === note._id
        );
        runInAction(() => {
          this.noteList.splice(index, 1);
        });
        this.updateToastsMessage({
          title: `Delete Note`,
          message: `${this.currentNote.title} deleted`,
        });
        this.currentNote = { ...this.emptyNote };
        this.selectedNoteIndex = -1;
      } else {
        this.updateToastsMessage({
          title: `Delete Note`,
          message: `Failed to Delete Note`,
        });
      }
    } else {
      this.updateToastsMessage({
        title: `Delete Note`,
        message: `Can't Delete Empty Note`,
      });
    }
  };

  updateKeyword = (keyword: string) => {
    this.keyword = keyword;
  };

  selectNote = (index: number) => {
    this.selectedNoteIndex = index;
    this.currentNote = { ...this.notes[this.selectedNoteIndex] };
    this.toastsMessage.show = false;
  };

  setCurrentNote = ({ title, content }: CurrentNoteProps) => {
    // console.log(typeof title, content);
    if (title !== undefined) this.currentNote.title = title;
    if (content !== undefined) this.currentNote.content = content;
  };

  updateToastsMessage = ({ title, message }: ToastsMessageProps) => {
    this.toastsMessage.show = false;
    this.toastsMessage.title = title;
    this.toastsMessage.message = message;
    this.toastsMessage.show = true;
  };

  closeToastsMessage = () => {
    this.toastsMessage.show = false;
  };

  reset = () => {
    this.noteList = [];
    this.keyword = "";
    this.selectedNoteIndex = -1;
    this.currentNote = { ...this.emptyNote };
    this.toastsMessage.show = false;
    this.toastsMessage.title = "";
    this.toastsMessage.message = "";
  };
}

export default NoteStore;
