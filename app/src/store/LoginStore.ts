import { MouseEvent } from "react";
import { makeAutoObservable, runInAction } from "mobx";

import { BaseUser, UserAuth, CurrentUser } from "../interface/User";
import { LoginApi } from "../api";
import { RootStore } from ".";

interface InputCheck {
  username: {
    isInvalid: boolean;
    feedback: string;
  };
  password: {
    isInvalid: boolean;
    feedback: string;
  };
}

class LoginStore {
  rootStore: RootStore;
  private _currentUser: CurrentUser = {
    _id: "",
    username: "",
    token: "",
    role: "",
    isLogged: false,
  };
  private _isLoading: boolean = false;
  private _inputCheck: InputCheck = {
    username: {
      isInvalid: false,
      feedback: "",
    },
    password: {
      isInvalid: false,
      feedback: "Password is Required.",
    },
  };

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
    makeAutoObservable(this);
    this.checkLogin();
  }

  get isLoading() {
    return this._isLoading;
  }

  set isLoading(value: boolean) {
    this._isLoading = value;
  }

  get inputCheck(): InputCheck {
    return this._inputCheck;
  }

  set inputCheck(value: InputCheck) {
    this._inputCheck = value;
  }

  get currentUser(): CurrentUser {
    return this._currentUser;
  }

  set currentUser(value: CurrentUser) {
    this._currentUser = value;
  }

  saveToken = async (token: string) => {
    await localStorage.setItem("token", token);
  };

  getToken = async () => {
    const token = await localStorage.getItem("token");
    if (token) {
      // console.log(token);
      // this.token = token;
      return token;
    }
    return null;
  };

  checkAdmin = (role: string): boolean => {
    return role === "admin" ? true : false;
  };

  checkLogin = async () => {
    const token = await this.getToken();
    if (token) {
      const userAuth = await LoginApi.checkLogin(token);
      if (userAuth) {
        if (this.checkAdmin(userAuth.role))
          this.rootStore.accountStore.fetchUser(userAuth.token);
        runInAction(() => {
          this.currentUser._id = userAuth._id;
          this.currentUser.username = userAuth.username;
          this.currentUser.token = userAuth.token;
          this.currentUser.role = userAuth.role;
          this.currentUser.isLogged = true;
        });
      }
    }
  };

  login = async (e: MouseEvent, { username, password, role }: BaseUser) => {
    runInAction(() => {
      if (username.length === 0) {
        this.inputCheck.username.feedback = "Username is Required.";
        this.inputCheck.username.isInvalid = true;
      } else {
        this.inputCheck.username.feedback = "";
        this.inputCheck.username.isInvalid = false;
      }
      if (password.length === 0) {
        this.inputCheck.password.feedback = "Password is Required.";
        this.inputCheck.password.isInvalid = true;
      } else {
        this.inputCheck.password.feedback = "";
        this.inputCheck.password.isInvalid = false;
      }
    });

    if (username.length === 0 || password.length === 0) {
      e.preventDefault();
    } else {
      this.isLoading = true;
      LoginApi.login({ username, password, role: "" }) // return current user
        .then(async (userAuth: UserAuth | null) => {
          if (userAuth) {
            if (this.checkAdmin(userAuth.role))
              this.rootStore.accountStore.fetchUser(userAuth.token);
            runInAction(() => {
              this.currentUser._id = userAuth._id;
              this.currentUser.username = userAuth.username;
              this.currentUser.token = userAuth.token;
              this.currentUser.role = userAuth.role;
              this.currentUser.isLogged = true;
            });
            await this.saveToken(userAuth.token);
          } else {
            runInAction(() => {
              this.inputCheck.password.feedback =
                "Incorrect Username Or Password";
              this.inputCheck.password.isInvalid = true;
            });
          }
          this.isLoading = false;
        })
        .catch((e) => {
          console.log(e.message);
        });
    }
  };

  logout = async () => {
    await localStorage.removeItem("token");
    runInAction(() => {
      this.currentUser._id = "";
      this.currentUser.username = "";
      this.currentUser.token = "";
      this.currentUser.role = "";
      this.currentUser.isLogged = false;
    });
    this.rootStore.noteStore.reset();
    this.rootStore.accountStore.reset();
  };
}

export default LoginStore;
