import { MouseEvent } from "react";
import { makeAutoObservable, runInAction } from "mobx";

import { RootStore } from ".";
import { UserApi } from "../api";
import { User } from "../interface/User";

interface NewPasswordInput {
  newPassword: string;
  confirmNewPassword: string;
}

interface UpdatePasswordCheck {
  newPassword: {
    isInvalid: boolean;
    feedback: string;
  };
  confirmNewPassword: {
    isInvalid: boolean;
    feedback: string;
  };
}

interface UserManagementCheck {
  username: {
    isInvalid: boolean;
    feedback: string;
  };
  password: {
    isInvalid: boolean;
    feedback: string;
  };
}

interface AlertMessage {
  isShow: boolean;
  variant: string;
  message: string;
}

class AccountStore {
  rootStore: RootStore;
  private _isLoading: boolean = false;
  private _newPasswordInput: NewPasswordInput = {
    newPassword: "",
    confirmNewPassword: "",
  };
  private _updatePasswordCheck: UpdatePasswordCheck = {
    newPassword: {
      isInvalid: false,
      feedback: "Password is Required.",
    },
    confirmNewPassword: {
      isInvalid: false,
      feedback: "Password is Required.",
    },
  };
  private _userManagementCheck: UserManagementCheck = {
    username: {
      isInvalid: false,
      feedback: "Username is Required.",
    },
    password: {
      isInvalid: false,
      feedback: "Password is Required.",
    },
  };
  private _alertMessage: AlertMessage = {
    isShow: false,
    variant: "",
    message: "",
  };
  private _users: User[] = [];
  private _selectedUserIndex: number = 0;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
    makeAutoObservable(this);
  }

  get newPasswordInput(): NewPasswordInput {
    return this._newPasswordInput;
  }

  set newPasswordInput(value: NewPasswordInput) {
    this._newPasswordInput = value;
  }

  get updatePasswordCheck(): UpdatePasswordCheck {
    return this._updatePasswordCheck;
  }

  set updatePasswordCheck(value: UpdatePasswordCheck) {
    this._updatePasswordCheck = value;
  }

  get userManagementCheck(): UserManagementCheck {
    return this._userManagementCheck;
  }

  set userManagementCheck(value: UserManagementCheck) {
    this._userManagementCheck = value;
  }

  get isLoading() {
    return this._isLoading;
  }

  set isLoading(value: boolean) {
    this._isLoading = value;
  }

  get alertMessage(): AlertMessage {
    return this._alertMessage;
  }

  set alertMessage(value: AlertMessage) {
    this._alertMessage = value;
  }

  get users(): User[] {
    return this._users;
  }

  set users(value: User[]) {
    this._users = value;
  }

  get selectedUserIndex(): number {
    return this._selectedUserIndex;
  }

  set selectUserIndex(value: number) {
    this._selectedUserIndex = value;
  }

  closeAlertMessage = () => {
    runInAction(() => {
      this.alertMessage.variant = "";
      this.alertMessage.message = "";
      this.alertMessage.isShow = false;
    });
  };

  updatePassword = async (
    e: MouseEvent,
    { newPassword, confirmNewPassword }: NewPasswordInput
  ) => {
    runInAction(() => {
      if (newPassword.length === 0) {
        this.updatePasswordCheck.newPassword.feedback =
          "New Password is Required.";
        this.updatePasswordCheck.newPassword.isInvalid = true;
      } else {
        this.updatePasswordCheck.newPassword.feedback = "";
        this.updatePasswordCheck.newPassword.isInvalid = false;
      }
      if (confirmNewPassword.length === 0) {
        this.updatePasswordCheck.confirmNewPassword.feedback =
          "Retype New Password is Required.";
        this.updatePasswordCheck.confirmNewPassword.isInvalid = true;
      } else {
        this.updatePasswordCheck.confirmNewPassword.feedback = "";
        this.updatePasswordCheck.confirmNewPassword.isInvalid = false;
      }
    });
    if (newPassword.length === 0 || confirmNewPassword.length === 0) {
      e.preventDefault();
    } else if (newPassword !== confirmNewPassword) {
      runInAction(() => {
        this.updatePasswordCheck.confirmNewPassword.feedback =
          "Password Don't Match";
        this.updatePasswordCheck.confirmNewPassword.isInvalid = true;
      });
      e.preventDefault();
    } else {
      this.isLoading = true;
      const user = await UserApi.updateUser(
        this.rootStore.loginStore.currentUser.token,
        this.rootStore.loginStore.currentUser._id,
        {
          username: this.rootStore.loginStore.currentUser.username,
          password: newPassword,
          role: this.rootStore.loginStore.currentUser.role,
        }
      );
      if (user) {
        this.isLoading = false;
        runInAction(() => {
          this.alertMessage.variant = "success";
          this.alertMessage.message = "Successful Password Update";
          this.alertMessage.isShow = true;
        });
        setTimeout(() => this.closeAlertMessage(), 4000);
      }
    }
  };

  fetchUser = async (token: string) => {
    const users = await UserApi.getUser(token);
    // console.log(users);
    if (users) {
      this.users = users;
    }
  };

  selectUser = (index: number) => {
    this.selectUserIndex = index;
  };

  addUser = async (user: User) => {
    if (user.username === "" || user.password === "") {
      runInAction(() => {
        if (user.username.length === 0) {
          this.userManagementCheck.username.feedback = "Username is Required.";
          this.userManagementCheck.username.isInvalid = true;
        } else {
          this.userManagementCheck.username.feedback = "";
          this.userManagementCheck.username.isInvalid = false;
        }
        if (user.password.length === 0) {
          this.userManagementCheck.password.feedback = "Password is Required.";
          this.userManagementCheck.password.isInvalid = true;
        } else {
          this.userManagementCheck.password.feedback = "";
          this.userManagementCheck.password.isInvalid = false;
        }
      });
    } else if (
      this.users.map(({ username }) => username).includes(user.username)
    ) {
      runInAction(() => {
        this.userManagementCheck.username.feedback = "User Already Exist.";
        this.userManagementCheck.username.isInvalid = true;
      });
    } else {
      const newUser = await UserApi.addUser(
        this.rootStore.loginStore.currentUser.token,
        user
      );
      if (newUser) {
        this.users.push(newUser);
        this.selectUserIndex = this.users.length - 1;
        runInAction(() => {
          this.alertMessage.variant = "success";
          this.alertMessage.message = `Successful Add User ${newUser.username}`;
          this.alertMessage.isShow = true;
        });
        setTimeout(() => this.closeAlertMessage(), 4000);
      }
    }
  };

  updateUser = async (user: User) => {
    if (user.username === "" || user.password === "") {
      runInAction(() => {
        if (user.username.length === 0) {
          this.userManagementCheck.username.feedback = "Username is Required.";
          this.userManagementCheck.username.isInvalid = true;
        } else {
          this.userManagementCheck.username.feedback = "";
          this.userManagementCheck.username.isInvalid = false;
        }
        if (user.password.length === 0) {
          this.userManagementCheck.password.feedback = "Password is Required.";
          this.userManagementCheck.password.isInvalid = true;
        } else {
          this.userManagementCheck.password.feedback = "";
          this.userManagementCheck.password.isInvalid = false;
        }
      });
    } else {
      const updatedUser = await UserApi.updateUser(
        this.rootStore.loginStore.currentUser.token,
        user._id,
        { username: user.username, password: user.password, role: user.role }
      );
      if (updatedUser) {
        const index = this.users.findIndex(
          (user) => user._id === updatedUser._id
        );
        this.users[index].username = updatedUser.username;
        this.users[index].password = updatedUser.password;
        this.users[index].role = updatedUser.role;
        runInAction(() => {
          this.alertMessage.variant = "success";
          this.alertMessage.message = `Successful Update User ${updatedUser.username}`;
          this.alertMessage.isShow = true;
        });
        setTimeout(() => this.closeAlertMessage(), 4000);
      }
    }
  };

  deleteUser = async (_id: string) => {
    if (_id === this.rootStore.loginStore.currentUser._id) {
      runInAction(() => {
        this.alertMessage.variant = "danger";
        this.alertMessage.message = `Can't Delete Current User`;
        this.alertMessage.isShow = true;
      });
      setTimeout(() => this.closeAlertMessage(), 4000);
    } else {
      const deletedUser = await UserApi.deleteUser(
        this.rootStore.loginStore.currentUser.token,
        _id
      );
      if (deletedUser) {
        const newUsers = this.users.filter(
          (user: User) => user._id !== deletedUser._id
        );

        this.selectUserIndex =
          this.selectUserIndex > 0 ? this.selectUserIndex - 1 : 0;
        this.users = newUsers;
        runInAction(() => {
          this.alertMessage.variant = "success";
          this.alertMessage.message = `Successful Delete User ${deletedUser.username}`;
          this.alertMessage.isShow = true;
        });
        setTimeout(() => this.closeAlertMessage(), 4000);
      }
    }
  };

  reset = () => {
    this.users = [];
    this.selectUserIndex = 0;
  };
}

export default AccountStore;
