import { createContext, useContext } from "react";

import NoteStore from "./NoteStore";
import LoginStore from "./LoginStore";
import AccountStore from "./AccountStore";

class RootStore {
  noteStore: NoteStore;
  loginStore: LoginStore;
  accountStore: AccountStore;
  constructor() {
    this.noteStore = new NoteStore(this);
    this.loginStore = new LoginStore(this);
    this.accountStore = new AccountStore(this);
  }
}

const StoreContext = createContext(new RootStore());
const useStore = () => useContext(StoreContext);

export { RootStore, useStore };
