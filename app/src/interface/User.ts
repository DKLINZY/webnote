export interface BaseUser {
  username: string;
  password: string;
  role: string;
}

export interface User extends BaseUser {
  _id: string;
}

export interface UserAuth {
  _id: string;
  username: string;
  token: string;
  role: string;
}

export interface CurrentUser {
  _id: string;
  username: string;
  token: string;
  role: string;
  isLogged: boolean;
}
