export interface CurrentNoteProps {
  title?: string;
  content?: string;
}
