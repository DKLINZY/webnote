export interface BaseNote {
  title: string;
  content?: string;
  createAt: Date;
  updateAt?: Date;
}

export interface Note extends BaseNote {
  _id: string;
}
