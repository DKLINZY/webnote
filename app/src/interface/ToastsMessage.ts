export interface ToastsMessage {
  show: boolean;
  title: string;
  message: string;
}

export interface ToastsMessageProps {
  title: string;
  message: string;
}
