# Server

- TypeScript
- Express
- JWT
- Mongoose

# App

- ReactJS
- TypeScript
- BootStrap
- Mobx

# Database

- MongoDB
