import React, { useState } from "react";
import { useObserver } from "mobx-react";
import { useStore } from "./store";

function App() {
  // here you can access all of the stores registered on the root store
  const { noteStore, userStore } = useStore();
  const [note, setNote] = useState("");

  // tracking the name change
  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    const {
      target: { value }
    } = e;
    // access the user store set name action
    userStore.setUserName(value);
  };

  // tracking the note change
  const handleNoteChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    const {
      target: { value }
    } = e;

    setNote(value);
  };

  const addNote = () => {
    // access the note store action adding note to the notes array
    noteStore.addNote(note);
  };

  // since we want to observe the change in name and list, useObserver is required, otherwise, we can straightaway return jsx
  return useObserver(() => (
    <div className="App">
      <h1>hello {userStore.name}</h1>

      <h2>Change your name here</h2>
      <input type="text" value={userStore.name} onChange={handleNameChange} />

      <h2>Insert note</h2>
      <input type="text" value={note} onChange={handleNoteChange} />
      <button type="button" onClick={addNote}>
        Add note
      </button>

      <h2>Note list</h2>
      {noteStore?.notes?.length ? (
        noteStore.notes.map((note, idx) => (
          <div key={idx}>
            <h3>from {note.username}</h3>
            <code>{note.note}</code>
          </div>
        ))
      ) : (
        <p>No note on the list</p>
      )}
    </div>
  ));
}

export default App;
