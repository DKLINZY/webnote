import { createContext, useContext } from "react";

import UserStore from "./user.store";
import NoteStore from "./note.store";

class RootStore {
  userStore: UserStore;
  noteStore: NoteStore;
  constructor() {
    this.userStore = new UserStore(this);
    this.noteStore = new NoteStore(this);
  }
}

const StoreContext = createContext(new RootStore());
const useStore = () => useContext(StoreContext);

export {RootStore, useStore}

//https://dev.to/cakasuma/using-mobx-hooks-with-multiple-stores-in-react-3dk4