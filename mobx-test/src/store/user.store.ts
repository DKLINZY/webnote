import { makeAutoObservable } from "mobx";
import { RootStore } from ".";

class userStore {
  name: string = "Shinya";

  constructor(rootStore: RootStore) {
    makeAutoObservable(this);
  }

  setUserName = (name: string) => {
    this.name = name;
  };
}

export default userStore;
