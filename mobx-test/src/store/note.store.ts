import { makeAutoObservable } from "mobx";
import { RootStore } from ".";

interface Note {
  username?: string;
  note: string;
}

class noteStore {
  rootStore: RootStore;
  notes: Note[] = [];

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
    makeAutoObservable(this);
  }

  addNote = (note: string) => {
    let _note: Note = { note };
    if (this.rootStore.userStore.name) {
      _note.username = this.rootStore.userStore.name;
    }
    this.notes.push(_note);
  };
}

export default noteStore;
